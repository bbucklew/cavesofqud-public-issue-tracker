# README #

### What is this repository for? ###

This is the public issue tracker for Caves of Qud.

If you have a problem, first check for an existing issue that matches your problem, and if one doesn't exist feel free to create a new issue describing your problem.

* [Go to the list of issues](https://bitbucket.org/bbucklew/cavesofqud-public-issue-tracker/issues?status=new&status=open)

### Who do I talk to? ###

* support@freeholdgames.com
* https://discord.com/invite/cavesofqud